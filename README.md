# VIDEO APP

## Run app using Docker

```bash
docker-compose up -d
```

Docker compose will download the required dependencies and start a new deployed instance of the application.

Application can be accessed via,

```
https://localhost:3000
```

## Getting Started for development

1. Go to `server` folder && Install Dependecies
```
npm install
```

2. Go to `server` folder and Copy `env.example` and rename to `.env`

```bash
cd server
```
```
PORT=8000

DB_NAME=video_app
DB_USER=postgres
DB_PASSWORD=postgres
DB_PORT=5439
DB_HOST=localhost

NODE_ENV = development
VIDEOS_PATH=http://localhost:8000/videos
THUMBNAILS_PATH=http://localhost:8000/thumbnails
```

2. Run database server using docker
```
npm run db:deploy
```

For local development,

3. Run the migrations
```
npm run migrate
```

4. Add data to DB
```
npm run seed
```

5. Run the server app
```
npm run dev
```

6. Go to `app` folder

```bash
cd ../app
```

7. Install the dependencies

```
yarn
```

8. Copy `env.example` and rename to `.env`

```
REACT_APP_API_URL=http://localhost:8000/api/
```

9. Run the application

```
yarn start
```