import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./components/Layout";
import VideoContextProvider from "./contexts/VideoContext";
import HomePage from "./pages/HomePage";
import UploadPage from "./pages/UploadPage";

function App() {
  return (
    <BrowserRouter>
      <VideoContextProvider>
        <Layout>
          <Routes>
            <Route index element={<HomePage />} />
            <Route path="upload" element={<UploadPage />} />
          </Routes>
        </Layout>
      </VideoContextProvider>
    </BrowserRouter>
  );
}

export default App;
