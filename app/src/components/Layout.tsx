import Navbar from "./navbar/Navbar";

const Layout = ({ children }: { children: JSX.Element }) => {
  return (
    <>
      <Navbar />
      <div className="container mx-auto">{children}</div>
    </>
  );
};

export default Layout;
