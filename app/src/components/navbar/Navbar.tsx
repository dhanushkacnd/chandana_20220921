import { Link } from "react-router-dom";
import UploadVideoButton from "./UploadVideoButton";

const Navbar = () => {
  return (
    <nav className="bg-white border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-900 h-16 shadow">
      <div className="container flex flex-wrap justify-between items-center mx-auto h-full">
        <div className="text-sky-500 font-bold text-xl">
          <Link to="/">Video App</Link>
        </div>
        <div className="w-full md:block md:w-auto" id="navbar-default">
          <UploadVideoButton />
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
