import { useNavigate } from "react-router-dom";

const UploadVideoButton = () => {
  const navigate = useNavigate();

  const onClick = () => {
    navigate("/upload");
  };

  return (
    <button
      className="bg-sky-500 rounded text-white px-4 py-2 hover:bg-sky-700 duration-500 transition-all"
      onClick={onClick}
    >
      Upload Video
    </button>
  );
};

export default UploadVideoButton;
