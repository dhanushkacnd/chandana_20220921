import { createContext, ReactNode, useState } from "react";
import useVideos from "../hooks/useVideo";
import { VideoRequest, VideoType } from "../types/video";

interface VideoContextType {
  videos: VideoType[];
  isModalOpen: boolean;
  closeModal: () => void;
  openModal: (videoId: number) => void;
  selectedVideo: VideoType | null;
  fetchVideos: () => void;
  saveVideo: (video: VideoRequest) => void;
}

const initialData: VideoContextType = {
  videos: [],
  isModalOpen: false,
  closeModal: () => {},
  openModal: () => {},
  selectedVideo: null,
  fetchVideos: () => {},
  saveVideo: () => {},
};

export const VideoContext = createContext<VideoContextType>(initialData);

const VideoContextProvider = ({ children }: { children: ReactNode }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { videos, fetchVideos, saveVideo } = useVideos();
  const [selectedVideo, setSelectedVideo] = useState<VideoType | null>(null);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const openModal = (videoId: number) => {
    const video = videos.find((v) => v.id === videoId);
    if (video) {
      setSelectedVideo(video);
      setIsModalOpen(true);
    }
  };

  return (
    <VideoContext.Provider
      value={{
        videos,
        isModalOpen,
        closeModal,
        openModal,
        selectedVideo,
        fetchVideos,
        saveVideo,
      }}
    >
      {children}
    </VideoContext.Provider>
  );
};

export default VideoContextProvider;
