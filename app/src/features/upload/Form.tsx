import { ChangeEvent, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import useCategories from "../../hooks/useCategories";
import useVideos from "../../hooks/useVideo";
import { isValidFileUploaded } from "../../utils/files";

const Form = () => {
  const [hasFileError, setFileError] = useState(false);
  const [selectedCategoryId, setSelectedCategoryId] = useState<number | null>(
    null
  );
  const [file, setFile] = useState<File | null>(null);
  const inputRef = useRef<HTMLInputElement>(null);

  const { categories, isError: isCategoryError, isLoading: isCategoryLoading } = useCategories();
  const { saveVideo, isError, isLoading } = useVideos();
  const navigate = useNavigate();

  const onUploadChange = (e: ChangeEvent<HTMLInputElement>) => {
    const files = e.target.files;
    if (!files || files.length < 1) {
      return;
    }

    const file = files[0];
    if (isValidFileUploaded(file)) {
      setFileError(false);
      setFile(file);
    } else {
      e.target.value = "";
      setFileError(true);
      setFile(null);
    }
  };

  const onChangeCategory = (e: ChangeEvent<HTMLSelectElement>) => {
    setSelectedCategoryId(Number(e.target.value));
  };

  const onSave = async () => {
    const title = inputRef.current?.value;

    if (!file || !title || !selectedCategoryId) return;

    const isSuccess = await saveVideo({
      title,
      categoryId: selectedCategoryId,
      file,
    });

    if (isSuccess) {
      navigate("/");
    }
  };

  return (
    <div className="mt-10">
      {(isCategoryError) && (
        <div className="px-5 text-center w-full rounded text-white bg-red-600 my-5">
          An error occurred while fetching the categories
        </div>
      )}
      {(isError) && (
        <div className="px-5 text-center w-full rounded text-white bg-red-600 my-5">
          An error occurred while saving the video
        </div>
      )}
      <div className="mb-5">
        <label
          htmlFor="title"
          className="block text-sm font-medium text-gray-700"
        >
          Video Title
        </label>
        <input
          type="text"
          name="title"
          id="title"
          ref={inputRef}
          className="border-gray-300 border ring-0 outline-none focus:border-sky-500 focus:outline-none focus:ring-1 focus:ring-sky-500 rounded px-2 py-1 text-lg w-full"
        />
      </div>
      <div className="mb-5">
        <label
          htmlFor="category"
          className="block text-sm font-medium text-gray-700"
        >
          Category
        </label>
        <select
          id="category"
          name="category"
          onChange={onChangeCategory}
          className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-3 shadow-sm focus:ring-1 focus:border-sky-500 focus:outline-none focus:ring-sky-500 sm:text-sm"
        >
          <option>Select category...</option>
          {categories.map((category) => (
            <option key={category.id} value={category.id}>
              {category.name}
            </option>
          ))}
        </select>
      </div>
      <div className="mb-5">
        <div className="flex justify-between">
          <label className="block text-sm font-medium text-gray-700 h-6">
            Upload video
          </label>
          {hasFileError && (
            <span className="bg-red-500 text-white px-2 py-1 rounded-full text-xs">
              Invalid file!
            </span>
          )}
        </div>
        <div className="mt-1 flex justify-center rounded-md border-2 border-dashed border-gray-300 px-6 pt-5 pb-6">
          <div className="space-y-1 text-center">
            <svg
              className="mx-auto h-12 w-12 text-gray-400"
              stroke="currentColor"
              fill="none"
              viewBox="0 0 48 48"
              aria-hidden="true"
            >
              <path
                d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <div className="flex text-sm text-gray-600 justify-center">
              <input
                id="file-upload"
                name="file-upload"
                type="file"
                accept=".mp4,.mov"
                onChange={onUploadChange}
              />
            </div>
            <p className="text-xs text-gray-500">MP4 or MOV up to 200MB</p>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <button
          className="bg-sky-500 px-4 py-2 rounded text-white disabled:cursor-not-allowed disabled:bg-sky-300"
          onClick={onSave}
          disabled={isLoading || isCategoryLoading || isCategoryError || isError }
        >
          {isLoading ? "Loading..." : "Save Video"}
        </button>
      </div>
    </div>
  );
};

export default Form;
