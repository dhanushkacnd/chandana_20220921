import { VideoType } from "../../../types/video";

interface VideoViewProps {
  video: VideoType;
  onSelect: () => void;
}

const VideoView = ({ video, onSelect }: VideoViewProps) => {
  const { title, category, thumbnailPath } = video;

  return (
    <div
      className="bg-white rounded shadow p-5 hover:cursor-pointer hover:bg-slate-100 transition-all duration-300"
      onClick={onSelect}
    >
      <div className="mb-2 font-semibold">{title}</div>
      <img className="rounded" src={thumbnailPath} alt={title} title={title} />
      <span className="rounded-full text-sm px-2 py-1 bg-sky-500 mt-5 inline-block text-white">
        {category.name}
      </span>
    </div>
  );
};

export default VideoView;
