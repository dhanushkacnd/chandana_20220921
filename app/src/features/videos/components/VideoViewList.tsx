import { isEmpty } from "lodash";
import { useContext } from "react";
import { VideoContext } from "../../../contexts/VideoContext";
import VideoView from "./VideoView";

const VideoViewList = () => {
  const { videos, openModal } = useContext(VideoContext);

  return (
    <section className="mt-5">
      {isEmpty(videos) && (
        <div className="mt-10 text-center text-gray-700 w-full text-sm">
          No videos available
        </div>
      )}
      <div className="grid grid-cols-5 gap-5">
        {videos.map((video) => (
          <VideoView
            key={video.id}
            video={video}
            onSelect={() => openModal(video.id)}
          />
        ))}
      </div>
    </section>
  );
};

export default VideoViewList;
