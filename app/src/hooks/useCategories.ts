import { useEffect, useState } from "react";
import { Category } from "../types/category";
import instance from "../utils/axios";

interface UseCategoriesType {
  categories: Category[];
  isLoading: boolean;
  isError: boolean;
}

const useCategories = (): UseCategoriesType => {
  const [categories, setCategories] = useState<Category[]>([]);
  const [isLoading, setLoading] = useState(false);
  const [isError, setError] = useState(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const { data } = await instance.get<Category[]>("/categories");
        setCategories(data);
        setLoading(false);
        setError(false);
      } catch (err) {
        setError(true);
        setLoading(false);
      }
    };

    fetchCategories();
  }, []);

  return { categories, isLoading, isError };
};

export default useCategories;
