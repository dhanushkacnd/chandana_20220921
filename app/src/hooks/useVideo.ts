import FormData from "form-data";
import { CREATED } from "http-status";
import omit from "lodash/omit";
import { useCallback, useState } from "react";
import { VideoRequest, VideoResponse, VideoType } from "../types/video";
import instance from "../utils/axios";

const useVideos = () => {
  const [videos, setVideos] = useState<VideoType[]>([]);
  const [isError, setError] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const saveVideo = async (data: VideoRequest) => {
    const { file, categoryId, title } = data;

    const formData = new FormData();
    formData.append("file", file);
    formData.append("title", title);
    formData.append("categoryId", String(categoryId));

    try {
      setLoading(true);

      const res = await instance.post<VideoRequest>("/videos", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      setLoading(false);

      return res.status === CREATED;
    } catch (err) {
      console.log(err);
      setError(true);
      setLoading(false);

      return false;
    }
  };

  const fetchVideos = useCallback(async () => {
    const { data } = await instance.get<VideoResponse[]>("/videos");
    const videos = data.map((d) => {
      const video = omit(d, "thumbnailPaths");
      return {
        ...video,
        thumbnailPath: d.thumbnailPaths["256x256"],
      };
    });
    setVideos(videos);
  }, []);

  return { saveVideo, videos, fetchVideos, isLoading, isError };
};

export default useVideos;
