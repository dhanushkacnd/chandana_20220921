import { useContext, useEffect } from "react";
import { VideoContext } from "../contexts/VideoContext";
import VideoModal from "../features/videos/components/VideoModal";
import VideoViewList from "../features/videos/components/VideoViewList";

const HomePage = () => {
  const { closeModal, isModalOpen, selectedVideo, fetchVideos } =
    useContext(VideoContext);

  useEffect(() => {
    fetchVideos();
  }, [fetchVideos]);

  return (
    <>
      <VideoViewList />
      {selectedVideo && (
        <VideoModal
          closeModal={closeModal}
          isModalOpen={isModalOpen}
          video={selectedVideo}
        />
      )}
    </>
  );
};

export default HomePage;
