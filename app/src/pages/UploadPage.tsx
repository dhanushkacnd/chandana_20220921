import Form from "../features/upload/Form";

const UploadPage = () => {
  return (
    <div className="max-w-lg mx-auto">
      <Form />
    </div>
  );
};

export default UploadPage;
