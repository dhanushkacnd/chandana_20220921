import { Category } from "./category";

export interface VideoType {
  id: number;
  title: string;
  srcPath: string;
  thumbnailPath: string;
  category: Category;
}

export interface VideoResponse {
  id: number;
  srcPath: string;
  category: Category;
  title: string;
  thumbnailPaths: { [key: string]: string };
}

export interface VideoRequest {
  title: string;
  file: File;
  categoryId: number;
}
