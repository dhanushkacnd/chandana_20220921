export const isValidFileUploaded = (file: File) => {
  const validExtensions = ["mp4", "mov"];
  const fileExtension = file.type.split("/")[1];
  return (
    validExtensions.includes(fileExtension) &&
    file.size < 200 * Math.pow(1024, 2)
  );
};
