import * as dotenv from "dotenv";
dotenv.config({ path: __dirname + "/../.env" });

const configuration = {
  env: process.env.NODE_ENV,
  port: process.env.PORT || 5000,
  database: process.env.DB_NAME,
  db_user: process.env.DB_USER,
  db_password: process.env.DB_PASSWORD,
  db_port: Number(String(process.env.DB_PORT)),
  db_host: process.env.DB_HOST,
  videos_path: process.env.VIDEOS_PATH,
  thumbnails_path: process.env.THUMBNAILS_PATH,
  thumbnail_size: ["64x64", "128x128", "256x256"] as const,
};

export default configuration;
