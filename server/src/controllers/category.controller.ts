import { NextFunction, Request, Response } from "express";
import { OK } from "http-status";
import { categoryService } from "../services/category.service";

class CategoryController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const categories = await categoryService.getAll();
      res.status(OK).json(categories);
    } catch (error) {
      next(error);
    }
  }
}

const categoryController = new CategoryController();
export { categoryController };
