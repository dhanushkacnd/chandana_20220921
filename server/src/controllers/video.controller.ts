import { NextFunction, Request, Response } from "express";
import { CREATED } from "http-status";
import { videoService } from "../services/video.service";
import { VideoDataType } from "../typings/video";

class VideoController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const videos = await videoService.getAll();
      res.json(videos);
    } catch (error: any) {
      next(error);
    }
  }

  async save(req: Request, res: Response, next: NextFunction) {
    try {
      const { title, categoryId } = req.body;
      const file = req.file?.filename as string;

      const data: VideoDataType = {
        file,
        category_id: categoryId,
        title,
      };

      await videoService.save(data);
      res.status(CREATED).end();
    } catch (error: any) {
      next(error);
    }
  }
}

const videoController = new VideoController();
export { videoController };
