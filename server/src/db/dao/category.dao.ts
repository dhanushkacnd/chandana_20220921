import Category from "../../models/Category";

export class CategoryDao {
  getAll() {
    return Category.query();
  }
}

const categoryDao = new CategoryDao();
export { categoryDao };
