import Video from "../../models/Video";
import { VideoDataType } from "../../typings/video";

export class VideoDao {
  getAll() {
    return Video.query().withGraphFetched("category");
  }

  save(data: VideoDataType & { thumbnails: { [key: string]: string } }) {
    return Video.query().insert(data);
  }
}

const videoDao = new VideoDao();
export { videoDao };
