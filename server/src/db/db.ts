import knex from "knex";
import { Model } from "objection";
import configuration from "./knexfile";
import config from "../config";

const connection = knex(configuration[config.env ?? "development"]);
Model.knex(connection);

export { Model };
