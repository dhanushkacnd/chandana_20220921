import type { Knex } from "knex";
import configuration from "../config";

const config: { [key: string]: Knex.Config } = {
  development: {
    client: "pg",
    connection: {
      database: configuration.database,
      user: configuration.db_user,
      password: configuration.db_password,
      port: configuration.db_port,
      host: configuration.db_host,
    },
    migrations: {
      directory: __dirname + "/migrations",
    },
    seeds: {
      directory: __dirname + "/seeds/dev",
    },
    useNullAsDefault: true,
  },
  production: {
    client: "pg",
    connection: "postgres://postgres:postgres@prod_db:5432/video_app",
    migrations: {
      directory: __dirname + "/migrations",
    },
    seeds: {
      directory: __dirname + "/seeds/dev",
    },
    useNullAsDefault: true,
    acquireConnectionTimeout: 300000,
  },
};

export default config;
