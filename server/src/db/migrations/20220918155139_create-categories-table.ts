import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await Promise.all([
    knex.schema.createTable("categories", (table) => {
      table.increments("id").primary();
      table.string("name");
    }),
  ]);
}

export async function down(knex: Knex): Promise<void> {
  await Promise.all([knex.schema.dropTable("categories")]);
}
