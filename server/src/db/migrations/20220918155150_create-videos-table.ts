import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await Promise.all([
    knex.schema.createTable("videos", (table) => {
      table.increments("id").primary();
      table.string("title");
      table.string("file");
      table.integer("category_id").references("categories.id");
    }),
  ]);
}

export async function down(knex: Knex): Promise<void> {
  await Promise.all([knex.schema.dropTable("videos")]);
}
