import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.table("videos", (table) => {
    table.jsonb("thumbnails");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.table("videos", (table) => {
    table.dropColumn("thumbnails");
  });
}
