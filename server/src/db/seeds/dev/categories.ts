import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("categories").del();

  await knex("categories").insert([
    { id: 1, name: "Exercise" },
    { id: 2, name: "Education" },
    { id: 3, name: "Recipe" },
  ]);
}
