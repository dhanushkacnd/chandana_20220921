"use strict";

import { NextFunction, Request, Response } from "express";
import { ValidationError } from "express-validation";
import httpStatus, { BAD_REQUEST, INTERNAL_SERVER_ERROR } from "http-status";
import { MulterError } from "multer";
import { FileTypeError } from "../errors/fileTypeError";

export const handleNotFound = (req: Request, res: Response) => {
  res
    .status(httpStatus.NOT_FOUND)
    .json({
      error: "Not found",
    })
    .end();
};

export const handleError = function (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (err instanceof ValidationError) {
    return res.status(err.statusCode).json(err);
  }

  if (err instanceof MulterError) {
    return res.status(BAD_REQUEST).json({ message: err.message });
  }

  if (err instanceof FileTypeError) {
    return res.status(BAD_REQUEST).json({ message: err.message });
  }

  return res.status(INTERNAL_SERVER_ERROR).json(err);
};
