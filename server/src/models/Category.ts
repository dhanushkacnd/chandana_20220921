import { Model } from "../db/db";
import Video from "./Video";

class Category extends Model {
  static get tableName() {
    return "categories";
  }

  static get relationMappings() {
    return {
      videos: {
        relation: Model.HasManyRelation,
        modelClass: Video,
        join: {
          from: "categories.id",
          to: "videos.category_id",
        },
      },
    };
  }
}

export default Category;
