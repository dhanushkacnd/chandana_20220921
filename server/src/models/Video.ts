import _ from "lodash";
import configuration from "../config";
import { Model } from "../db/db";
import Category from "./Category";

class Video extends Model {
  static get tableName() {
    return "videos";
  }

  static get virtualAttributes() {
    return ["srcPath", "thumbnailPaths"];
  }

  //@ts-ignore
  $formatJson(json) {
    json = super.$formatJson(json);
    return _.omit(json, ["category_id", "file", "thumbnails"]);
  }

  srcPath() {
    //@ts-ignore
    return `${configuration.videos_path}/${this.file}`;
  }

  thumbnailPaths() {
    //@ts-ignore
    const thumbnails = this.thumbnails;
    const values = Object.entries(thumbnails).reduce<{ [key: string]: string }>(
      (accum, [k, v]) => {
        return { ...accum, [k]: `${configuration.thumbnails_path}/${v}` };
      },
      {}
    );
    return values;
  }

  static get relationMappings() {
    return {
      category: {
        relation: Model.BelongsToOneRelation,
        modelClass: Category,
        join: {
          from: "videos.category_id",
          to: "categories.id",
        },
      },
    };
  }
}

export default Video;
