import express from "express";
import videoRouter from "./video.route";
import categoryRouter from "./category.route";

const router = express.Router();
router.use("/videos", videoRouter);
router.use("/categories", categoryRouter);

export default router;
