import express from "express";
import { videoController } from "../../controllers/video.controller";
import uploadHandler from "../../utils/storage";
import { validateSave } from "../../validation/video.validation";

const router = express.Router();

router.get("/", videoController.getAll);
router.post("/", uploadHandler, validateSave, videoController.save);

export default router;
