import { categoryDao } from "../db/dao/category.dao";

class CategoryService {
  getAll() {
    return categoryDao.getAll();
  }
}

const categoryService = new CategoryService();
export { categoryService };
