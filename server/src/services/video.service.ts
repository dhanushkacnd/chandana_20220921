import { videoDao } from "../db/dao/video.dao";
import { ThumbnailSize, VideoDataType } from "../typings/video";
import { generateThumbnails, getFileName } from "../utils";

class VideoService {
  getAll() {
    return videoDao.getAll();
  }

  async save(data: VideoDataType) {
    const sizes: ThumbnailSize[] = ["64x64", "128x128", "256x256"];
    const fileName = getFileName(data.file);

    const result = await generateThumbnails(data.file, sizes);

    if (result.every((e) => e)) {
      const thumbnails = sizes.reduce((accum, s) => {
        return { ...accum, [s]: `${fileName}/thumbnail-${s}.png` };
      }, {});

      return videoDao.save({ ...data, thumbnails });
    } else {
      throw new Error("Failed to create thumbnails");
    }
  }
}

const videoService = new VideoService();
export { videoService };
