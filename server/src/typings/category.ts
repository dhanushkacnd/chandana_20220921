export interface CategoryDataType {
  name: string;
}

export interface CategoryType {
  id: number;
  name: string;
}
