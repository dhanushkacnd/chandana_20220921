import configuration from "../config";
import { CategoryType } from "./category";
export interface VideoDataType {
  title: string;
  category_id: string;
  file: string;
}

export interface VideoType {
  id: number;
  title: string;
  category: CategoryType;
  srcPath: string;
}

const sizes = configuration.thumbnail_size;
export type ThumbnailSize = typeof sizes[number];
