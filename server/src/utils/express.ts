import cors from "cors";
import express, { Express } from "express";
import morgan from "morgan";
import configuration from "../config";

import { handleError, handleNotFound } from "../middlewares/error-handler";
import apiRouter from "../routes/api";

const app: Express = express();

app.use(cors());
app.use(morgan("combined"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/videos", express.static("dist/uploads"));
app.use("/thumbnails", express.static("dist/thumbnails"));

app.use("/api", apiRouter);

app.use(handleNotFound);
app.use(handleError);

const start = () => {
  app.listen(configuration.port, () => {
    console.log(`App is running on ${configuration.port}`);
  });
};

export default start;
