export const getFileName = (file: string) =>
  file.slice(0, file.lastIndexOf("."));
