import { NextFunction, Request } from "express";
import multer from "multer";
import path from "path";
import { FileTypeError } from "../errors/fileTypeError";
import { v4 } from "uuid";

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + "../../../uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, v4() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 200 * Math.pow(1024, 2) },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "video/mp4" || file.mimetype == "video/mov") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new FileTypeError("Only .mp4 and .mov files are supported"));
    }
  },
});

const uploadHandler = (req: Request, res: any, next: NextFunction) => {
  upload.single("file")(req, res, (err) => {
    return next(err);
  });
};

export default uploadHandler;
