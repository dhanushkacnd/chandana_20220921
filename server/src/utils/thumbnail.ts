import ffmpeg from "fluent-ffmpeg";
import fs from "fs";
import path from "path";
import { ThumbnailSize } from "../typings/video";
import { getFileName } from "./file";

const generateThumbnails = (file: string, sizes: ThumbnailSize[]) => {
  const fileName = getFileName(file);
  const folder = path.join(__dirname, "/../../thumbnails/", fileName);

  fs.mkdirSync(folder);

  const promises = sizes.map((size) => {
    return new Promise<boolean>((resolve, reject) => {
      ffmpeg(path.join(__dirname, "/../../uploads", file))
        .thumbnails({
          timestamps: ["10%"],
          filename: `thumbnail-${size}.png`,
          folder,
          size,
        })
        .on("end", () => {
          resolve(true);
        })
        .on("error", (err) => {
          return reject(false);
        });
    });
  });
  return Promise.all(promises);
};

export default generateThumbnails;
