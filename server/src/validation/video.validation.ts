import { NextFunction, Request, Response } from "express";
import { Joi } from "express-validation";
import { BAD_REQUEST } from "http-status";

export const videoSchema = Joi.object({
  title: Joi.string().required(),
  categoryId: Joi.number().required(),
});

export const validateSave = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { error } = videoSchema.validate(req.body);

  const messages = {
    missing: 'Required parameter "file" is missing',
    invalidSchema: error?.details[0].message,
  };

  const hasFile = !!req.file;

  let errorMessages: (string | undefined)[] = [];
  if (!hasFile) {
    errorMessages = [...errorMessages, messages["missing"]];
  }

  if (error) {
    errorMessages = [...errorMessages, messages["invalidSchema"]];
  }

  if (error || !hasFile) {
    errorMessages = errorMessages.filter((e) => !!e);
    res.status(BAD_REQUEST).json({ error: "Validation failed", errorMessages });
  } else {
    next();
  }
};
